package ch.zhaw.cas.oop.prototyping;

import java.util.ArrayList;
import java.util.Iterator;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

/**
* bla bla test
*
* @author  D. Schmidt
* @version 1.0
*/

//TODO  Muss als sep. thread laufen.
// Bekommt cmd und fuehrt transition aus. ruft entry, cyclic und exit auf.
public class StateMachine extends Thread {

	private ArrayList<FState> states;
	private FState previousState = null;
	// TODO Was ist im Fall von einer verschachtelten State machine? Braucht es eine Liste?
	private FState actualState = null;
	private FState nextState = null;
	private String next;
	private ExecuterState executerState = ExecuterState.IDLE;
	//private static final Logger logger = LogManager.getLogger(StateMachine.class.getName());

	public StateMachine(){
		states = new ArrayList<FState>();
	}

	public void addState(FState state) {
		states.add(state);
	}
	
	public void start(){
		if(super.getState() == Thread.State.NEW){
			super.start();
		}
	}
	
	public void setNext(String next){
		this.next = next;
	} 
	
	private String getNext(){
		return next;
	} 
	
	public boolean hasStates(){
		return !states.isEmpty();
	} 
	
	public int getNumberOfStates(){
		return states.size();
	}
	
	public ArrayList<FState> getStates(){
		return states;
	}
	
	public FState getPreviousState(){
		return previousState;
	}
	
	public FState getActualState(){
		return actualState;
	}
	
	public FState getNextState(){
		return nextState;
	}
	
	private void setPreviousState(FState state){
		this.previousState = state;
	}
	
	private void setActualState(FState state){
		this.actualState = state;
	}
	
	private void setNextState(FState state){
		this.nextState = state;
	}
	

	@Override
	public void run() {
	
		// Start endless loop
		while(true){	
		
			switch (executerState)
			{
				case IDLE:
				//	logger.trace(super.getName() + ":" + ExecuterState.IDLE.toString());
					executerState = ExecuterState.INIT;
		    	break;
		    	
				case INIT:
					System.out.println(super.getName() + ":" + ExecuterState.INIT.toString());
					// Call initialize method of all states from this object
					for (FState state: getStates()) {
					    state.initialize();
					}
					executerState = ExecuterState.NEXT;
			    break;
		    	
				case NEXT:
					System.out.println(super.getName() + ":" + ExecuterState.NEXT.toString());
					
					if(getNextState() != null){
						setPreviousState(getActualState());
						setActualState(getNextState());
					}
					
					// First initial step?
					if(previousState == null){
						setActualState(states.get(0));
					}
								
					executerState = ExecuterState.ENTRY;
			    break;
			    
				case ENTRY:
					System.out.println(super.getName() + ":" + ExecuterState.ENTRY.toString());
					// call entry
					getActualState().entry();
					// next cyclic
					executerState = ExecuterState.CYCLIC;						
				break;
			    
				case CYCLIC:
					System.out.println(super.getName() + ":" + ExecuterState.CYCLIC.toString());
					// TODO call cyclic as long as next transition
					// call cyclic
					getActualState().cyclic();
		
					// next transition
					if(!getNext().equals("")){
						executerState = ExecuterState.EXIT;				
					}
					
				break;
				
				case EXIT:
					System.out.println(super.getName() + ":" + ExecuterState.EXIT.toString());
					// TODO call exit of actual state
					// call exit
					getActualState().exit();
					
					
					boolean found = false;
					// Iterator over the valid transitions of actual state
					Iterator<Transition> ittrans = getActualState().getTransitions().iterator();
					
					while (ittrans.hasNext() && !found) {
						Transition transition = ittrans.next();
						if (transition.getStateNext().getName().equals(getNext())){
							// Set next state
							setNextState(transition.getStateNext());
							found = true;							    
						}
					}
					
					if(found){
						// Reset the command
						setNext("");
						executerState = ExecuterState.NEXT;
					}
					else{
						// TODO: exception behandeln
						System.out.println("Invalid transition: " + getActualState().getName() + " to " +  getNext());
						executerState = ExecuterState.ERROR;
					}
					
					
					// TODO Anhand von dem Beispielmmodell muesste hier folgendes passieren:
					// Wenn die Transition State 1 nach State 2 getriggert wird (valid) muss entry von State 2 dann von State 3 ausgefuehrt werden.
					// Cyclic wird dann sowohl von State 2 als auch State 3 ausgefuehrt.
					// Heisst: es braucht eine Liste "actualStates" da immer mehrere States zeitgleich activ sein koennen.
					// Eine Statemachine ohne die Behandlung von SubStates funktioniert bereits.
					
					
				break;
				
				case FINAL:
				break;
				
				case ERROR:
					// TODO wohin jetzt?
					// Abbruch tread?
				break;
				
				default:
				break;
				
			}
		
		}
	}
	
	
}