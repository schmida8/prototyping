package ch.zhaw.cas.oop.prototyping;

/**
* bla bla
*
* @author  D. Schmidt
* @version 1.0
*/
public interface Executable {

	// Called only one time after startup from state machine executer
	public void initialize();
	
	public void entry();
	
	public void cyclic();
	
	public void exit();
}