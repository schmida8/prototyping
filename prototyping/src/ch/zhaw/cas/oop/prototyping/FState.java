package ch.zhaw.cas.oop.prototyping;

import java.util.ArrayList;

/**
* bla bla bla
*
* @author  D. Schmidt
* @version 1.0
*/
public class FState implements Executable{

	private String id;
	private String name;
	private boolean initial = false;
	
	// Reference to the corresponding state machine that use this state
	private StateMachine stateMachine;
	// List of transitions
	private ArrayList<Transition> transitions;
	// List of substates
	private ArrayList<FState> substates;
	
	public FState(StateMachine stateMachine, String id, String name, boolean initial){
		this.stateMachine = stateMachine;
		this.id = id;
		this.name = name;
		this.initial = initial;
		substates = new ArrayList<FState>(); 
		transitions = new ArrayList<Transition>(); 
	}
	
	/**
	 * Returns the state ID
	 * 
	 * @return State ID
	 */
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public StateMachine getStateMachine() {
		return stateMachine;
	}
	
	/**
	 * Add substate to this state
	 * 
	 * @param substate 
	 */
	public void addSubstate(FState substate) {
		substates.add(substate);
	}
	
	/**
	 * Add transition to this state
	 * 
	 * @param transition 
	 */
	public void addTransition(Transition transition) {
		transitions.add(transition);
	}
	
	/**
	 * Get list with all valid transitions
	 * 
	 * @return transitions 
	 */
	public ArrayList<Transition> getTransitions(){
		return transitions;
	}
	
	public ArrayList<FState> getSubstates(){
		return substates;
	}
	
	/**
	 * Get back information if state is an composite state.
	 * Generally, composite state is defined as state that has substates. 
	 * 
	 * @return <code>true</code> if state is an composite state
	 */
	public boolean isCompositeState(){
		return !substates.isEmpty();
	} 
	
	public boolean isInitial(){
		return initial;
	} 
	
	public int getNumberOfSubstates(){
		return substates.size();
	}
	
	public synchronized void initialize(){};
	

	public synchronized void entry(){};
	

	public synchronized void cyclic(){};
	

	public synchronized void exit(){}
}