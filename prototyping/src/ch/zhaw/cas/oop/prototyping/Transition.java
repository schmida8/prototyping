package ch.zhaw.cas.oop.prototyping;

/**
* bla bla
*
* @author  D. Schmidt
* @version 1.0
*/
public class Transition {
	
	private FState stateNext;
	
	public Transition(FState stateNext){
		this.stateNext = stateNext;
	}

	public FState getStateNext() {
		return stateNext;
	}
}