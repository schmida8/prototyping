package ch.zhaw.cas.oop.prototyping;

public class ConcreteState6 extends FState{

	public ConcreteState6(StateMachine stateMachine, String id, String name, boolean initial) {
		super(stateMachine, id, name, initial);
	}
	
	@Override
	public synchronized void initialize(){
		System.out.println(this.getName() + " initialize");
	};
	
	
	@Override
	public synchronized void entry(){
		System.out.println(this.getName() + " entry");
	};
	
	
	@Override
	public synchronized void cyclic(){
		System.out.println(this.getName() + " cyclic");
		
		// TODO transition test
		// Darf nicht asugefuehrt werden, da keine direkte transition besteht.
		super.getStateMachine().setNext("State2");
	};
	
	@Override
	public synchronized void exit(){
		System.out.println(this.getName() + " exit");
	};
	
}