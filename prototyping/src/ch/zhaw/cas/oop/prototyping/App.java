package ch.zhaw.cas.oop.prototyping;


public class App {

	private static ConcreteUnit1 concreteUnit1;
	
	public static void main(String[] args) {

		// Create an concrete unit to handle states
		concreteUnit1 = new ConcreteUnit1("1","Axis controller");
		
		// Create all necessary states for concrete unit
		ConcreteState1 concreteState1 = new ConcreteState1(concreteUnit1,"1","State1",true);
		ConcreteState2 concreteState2 = new ConcreteState2(concreteUnit1,"2","State2",false);
		ConcreteState3 concreteState3 = new ConcreteState3(concreteUnit1,"3","State3",false);
		ConcreteState4 concreteState4 = new ConcreteState4(concreteUnit1,"3-4","State4",true);
		ConcreteState5 concreteState5 = new ConcreteState5(concreteUnit1,"3-5","State5",false);
		ConcreteState6 concreteState6 = new ConcreteState6(concreteUnit1,"6","State6",false);
		
		// Add State4 and State5 as substates of State3
		concreteState3.addSubstate(concreteState4);
		concreteState3.addSubstate(concreteState5);
	
		// Add transistions
		concreteState1.addTransition(new Transition(concreteState2));
		concreteState2.addTransition(new Transition(concreteState3));
		concreteState4.addTransition(new Transition(concreteState5));
		concreteState4.addTransition(new Transition(concreteState6));
		concreteState5.addTransition(new Transition(concreteState4));
		concreteState6.addTransition(new Transition(concreteState5));
		
		// Registrate all existing states to concrete unit 1
		concreteUnit1.addState(concreteState1);
		concreteUnit1.addState(concreteState2);
		concreteUnit1.addState(concreteState3);
		concreteUnit1.addState(concreteState4);
		concreteUnit1.addState(concreteState5);
		concreteUnit1.addState(concreteState6);
		
		// Start execution of internal state machine
		concreteUnit1.start();
		
		// Test methods of unit
		System.out.println(concreteUnit1.getNumberOfStates());
		System.out.println(concreteUnit1.hasStates());
		
		
		for (FState state: concreteUnit1.getStates()) {
		    System.out.println(state.getName());
		}
		
		
		//while(true){;}
	}

}
