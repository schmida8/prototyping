package ch.zhaw.cas.oop.prototyping;

/**
* bla bla
*
* @author  D. Schmidt
* @version 1.0
*/
public enum ExecuterState {
  IDLE,
  INIT,
  NEXT,
  ENTRY,
  CYCLIC,
  EXIT,
  FINAL,
  ERROR
}