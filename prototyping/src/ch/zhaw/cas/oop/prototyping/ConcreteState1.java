package ch.zhaw.cas.oop.prototyping;

public class ConcreteState1 extends FState{

	public ConcreteState1(StateMachine stateMachine, String id, String name, boolean initial) {
		super(stateMachine, id, name, initial);
	}

	
	@Override
	public synchronized void initialize(){
		System.out.println(this.getName() + " initialize");
	};
	
	
	@Override
	public synchronized void entry(){
		System.out.println(this.getName() + " entry");
	};
	
	
	@Override
	public synchronized void cyclic(){
		System.out.println(this.getName() + " cyclic");
		
		
		//TODO IO solution Silvio !!!! 
		// if(objXY.getInput){;}
		
		

		// TODO sleep just for test
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO sleep just for test
		
		// TODO transition test
		super.getStateMachine().setNext("State2");
		
		
		
		
		
	};
	
	@Override
	public synchronized void exit(){
		System.out.println(this.getName() + " exit");
	};
	
}