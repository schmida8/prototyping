package ch.zhaw.cas.oop.prototyping;

/**
* bla bla
*
* @author  D. Schmidt
* @version 1.0
*/
public class ConcreteUnit extends StateMachine{

	private String id;
	private String name;
	// TODO hier müsste Liste mit Childs sein
	// private ArrayList<Unit>.....

	public ConcreteUnit(String id, String name){
		this.id = id;
		this.name = name;
	}
	
	public String getUnitId() {
		return id;
	}

	public String getUnitName() {
		return name;
	}
	
	public void addState(FState state) {
		super.addState(state);
	}
	
}